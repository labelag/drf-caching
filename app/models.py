from django.db import models

# Create your models here.
class timeStamp(models.Model):
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)

    def get_page(self):
        return self._default_manager.filter(id=self.pk).count() / 10 + 1



from .models import timeStamp
from rest_framework import serializers
from rest_framework.pagination import PaginationSerializer
class TimeStampSerializer(serializers.ModelSerializer):
    class Meta:
        model = timeStamp


from django.conf.urls import patterns, url
from .views import TimeStampApiView
urlpatterns = patterns('app.views',
    url(r'^api/$', TimeStampApiView.as_view(),name="timestampapi"),
)
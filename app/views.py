from django.template import RequestContext
from .models import timeStamp
from .serializers import TimeStampSerializer
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status

from django.core.urlresolvers import reverse
from .mixins import CachedResourceMixin
import requests
import math
class TimeStampApiView(CachedResourceMixin,ListAPIView):
    """
    List all snippets, or create a new snippet.
    """
    queryset = timeStamp.objects.all()
    paginate_by = 10
    serializer_class = TimeStampSerializer


    def post(self, request, format=None):
        serializer = TimeStampSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            object_id = serializer.data['id']
            page_position = ((timeStamp.objects.filter(pk__lt=object_id).count()) / self.paginate_by) + 1;
            self.clear_cache_response(page=page_position)
            if page_position > 1:
                requests.get("http://localhost:8000"+reverse("timestampapi")+"?page="+str(page_position))
            else:
                requests.get("http://localhost:8000"+reverse("timestampapi"))

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
